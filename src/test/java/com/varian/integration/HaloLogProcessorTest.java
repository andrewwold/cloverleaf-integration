package com.varian.integration;


import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.varian.integration.converters.IntegrationMessageConverter;
import com.varian.integration.dto.IntegrationMessage;
import com.varian.integration.dto.SaveResponse;
import com.varian.integration.frameworks.retrofit.HaloConnectLogServiceClient;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class HaloLogProcessorTest {

    private HaloLogProcessor haloLogProcessor;

    @Mock
    private HaloConnectLogServiceClient haloConnectLogServiceClient;

    @Mock
    private IntegrationMessageConverter integrationMessageConverter;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void test_nullIntegrationMessageConverter() {
        exception.expect(IllegalArgumentException.class);
        haloLogProcessor = new HaloLogProcessor(null, haloConnectLogServiceClient);
    }

    @Test
    public void test_nullHaloConnectLogServiceClient() {
        exception.expect(IllegalArgumentException.class);
        haloLogProcessor = new HaloLogProcessor(integrationMessageConverter, null);
    }

    @Test
    public void test_called() throws CloverleafException {
        haloLogProcessor = new HaloLogProcessor(integrationMessageConverter, haloConnectLogServiceClient);

        haloLogProcessor.processMessage();

        verify(integrationMessageConverter, times(1)).getIntegrationMessage();
    }

    @Test
    public void test_errorInWebServiceCall_ShouldNotThrowException() throws Exception {
        when(haloConnectLogServiceClient.logIntegrationMessage(any(IntegrationMessage.class))).thenThrow(Exception.class);

        haloLogProcessor = new HaloLogProcessor(integrationMessageConverter, haloConnectLogServiceClient);
        haloLogProcessor.processMessage();

        //verify(cloverEnv, times(1)).notifyWarning(anyString());
    }

    @Test
    public void test_WebServiceReturnsPositiveInt() throws Exception {
        SaveResponse saveResponse = mock(SaveResponse.class);
        when(saveResponse.getId()).thenReturn(1L);
        when(haloConnectLogServiceClient.logIntegrationMessage(any(IntegrationMessage.class))).thenReturn(saveResponse);

        haloLogProcessor = new HaloLogProcessor(integrationMessageConverter, haloConnectLogServiceClient);
        haloLogProcessor.processMessage();
    }

    @Test
    public void test_WebServiceReturnsnegativeInt() throws Exception {
        SaveResponse saveResponse = mock(SaveResponse.class);
        when(saveResponse.getId()).thenReturn(-1L);
        when(haloConnectLogServiceClient.logIntegrationMessage(any(IntegrationMessage.class))).thenReturn(saveResponse);

        haloLogProcessor = new HaloLogProcessor(integrationMessageConverter, haloConnectLogServiceClient);
        haloLogProcessor.processMessage();
    }
}

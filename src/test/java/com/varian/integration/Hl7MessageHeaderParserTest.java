package com.varian.integration;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Hl7MessageHeaderParserTest {
    private Hl7MessageHeaderParser hl7MessageHeaderParser;

    private static final String validHl7 = "MSH|^~\\&|EXTERNAL|HIS|MEDONC_ADT0|HIS|201507161823||ADT^A04|000008829|P|2.5.1||||||UNICODE UTF-8|||";

    @Test
    public void test_validHL7Message() {
        hl7MessageHeaderParser = new Hl7MessageHeaderParser(validHl7);

        String id = hl7MessageHeaderParser.getMessageHeaderId();
        String eventCode = hl7MessageHeaderParser.getEventCode();

        assertEquals("000008829", id);
        assertEquals("ADT^A04", eventCode);
    }

    @Test
    public void test_invalidHl7_returnsNull() {
        hl7MessageHeaderParser = new Hl7MessageHeaderParser("invalidhl7");

        String id = hl7MessageHeaderParser.getMessageHeaderId();
        String eventCode = hl7MessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }

    @Test
    public void test_emptyMessage_returnsNull() {
        hl7MessageHeaderParser = new Hl7MessageHeaderParser("");

        String id = hl7MessageHeaderParser.getMessageHeaderId();
        String eventCode = hl7MessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }

    @Test
    public void test_nullMessage_returnsNull() {
        hl7MessageHeaderParser = new Hl7MessageHeaderParser(null);

        String id = hl7MessageHeaderParser.getMessageHeaderId();
        String eventCode = hl7MessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }
}

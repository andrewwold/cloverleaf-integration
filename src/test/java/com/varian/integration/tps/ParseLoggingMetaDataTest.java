package com.varian.integration.tps;

import com.quovadx.cloverleaf.upoc.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class ParseLoggingMetaDataTest {
    private ParseLoggingMetaData parseLoggingMetaData;
    private String validFHIRXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Bundle xmlns=\"http://hl7.org/fhir\"> <type value=\"message\"/> <base value=\"http://varian.com/halo\"/> <entry>  <resource>   <MessageHeader xmlns=\"http://hl7.org/fhir\">    <!-- JMS Event ID -->    <id value=\"000020001\"/>    <identifier value=\"000020001\"/>    <timestamp value=\"2015-08-31T12:32:43Z\"/>    <event>     <system value=\"http://hl7.org/fhir/message-events\"/>     <code value=\"diagnosis-create\"/>    </event>    <source>     <name value=\"HALO_ADT\"/>     <software value=\"HALO\"/>     <!-- Placeholder for now -->     <endpoint value=\"http://varian.com/halo\"/>    </source>    <data>     <reference value=\"Condition/c1gfggggg\"/>    </data>    <data>     <reference value=\"Condition/c2\"/>    </data>    <data>     <reference value=\"Condition/c3\"/>    </data> </MessageHeader></resource> </entry> <entry>  <resource>";
    private String validHL7 = "MSH|^~\\&|EXTERNAL|HIS|RADONC||200401300539||ADT^A08|19391|P|2.5.1||||||UNICODE UTF-8|||";
    @Mock
    private CloverEnv cloverEnv;

    @Mock
    private PropertyTree xargs;

    @Mock
    private Message message;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() throws CloverleafException {
        parseLoggingMetaData = new ParseLoggingMetaData(cloverEnv, xargs);
    }

    @Test
    public void test_nullMessage_doesNotThrowError() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        parseLoggingMetaData.process(cloverEnv, "", "run", null);
    }

    @Test
    public void test_incompleteXMLDoesNotThrowException() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenReturn(new PropertyTree());
        when(message.getContent(anyInt(), anyInt())).thenReturn("<Bundle />");
        parseLoggingMetaData.process(cloverEnv, "", "run", message);
    }

    @Test
    public void test_validFHIRXML() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenReturn(new PropertyTree());
        when(message.getContent(anyInt(), anyInt())).thenReturn(validFHIRXml);

        DispositionList dispList = parseLoggingMetaData.process(cloverEnv, "", "run", message);
        assertEquals(1, dispList.length());
        assertTrue(dispList.contains(message));
    }

    @Test
    public void test_validHL7XML() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenReturn(new PropertyTree());
        when(message.getContent(anyInt(), anyInt())).thenReturn(validHL7);

        DispositionList dispList = parseLoggingMetaData.process(cloverEnv, "", "run", message);
        assertEquals(1, dispList.length());
        assertTrue(dispList.contains(message));
    }

    @Test
    public void test_destoryParseLoggingMetaDta_doesNotThrowException() throws CloverleafException {
        parseLoggingMetaData.destroy(cloverEnv);
    }
}

package com.varian.integration.tps;

import com.quovadx.cloverleaf.upoc.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HaloLogTest {
    private HaloLog haloLog;

    @Mock
    private CloverEnv cloverEnv;

    @Mock
    private PropertyTree xargs;


    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() throws CloverleafException {
    }

    @Test
    public void test_validMessage() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(new PropertyTree());
        when(xargs.getString("StatusId")).thenReturn("InProgress");

        haloLog = new HaloLog(cloverEnv, xargs);

        DispositionList dispList = haloLog.process(cloverEnv, "", "run", message);
        assertEquals(1, dispList.length());
        assertTrue(dispList.contains(message));
    }

    @Test
    public void test_destoryHaloLog_doesNotThrowException() throws CloverleafException {
        haloLog = new HaloLog(cloverEnv, xargs);
        haloLog.destroy(cloverEnv);
    }
}

package com.varian.integration;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class XmlMessageHeaderParserTest {
    private XmlMessageHeaderParser xmlMessageHeaderParser;

    private static final String validFHIRXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Bundle xmlns=\"http://hl7.org/fhir\"> <type value=\"message\"/> <base value=\"http://varian.com/halo\"/> <entry>  <resource>   <MessageHeader xmlns=\"http://hl7.org/fhir\">    <!-- JMS Event ID -->    <id value=\"000020001\"/>    <identifier value=\"000020001\"/>    <timestamp value=\"2015-08-31T12:32:43Z\"/>    <event>     <system value=\"http://hl7.org/fhir/message-events\"/>     <code value=\"diagnosis-create\"/>    </event>    <source>     <name value=\"HALO_ADT\"/>     <software value=\"HALO\"/>     <!-- Placeholder for now -->     <endpoint value=\"http://varian.com/halo\"/>    </source>    <data>     <reference value=\"Condition/c1gfggggg\"/>    </data>    <data>     <reference value=\"Condition/c2\"/>    </data>    <data>     <reference value=\"Condition/c3\"/>    </data> </MessageHeader></resource> </entry> <entry>  <resource>";

    @Test
    public void test_validHL7Message() {
        xmlMessageHeaderParser = new XmlMessageHeaderParser(validFHIRXml);

        String id = xmlMessageHeaderParser.getMessageHeaderId();
        String eventCode = xmlMessageHeaderParser.getEventCode();

        assertEquals("000020001", id);
        assertEquals("diagnosis-create", eventCode);
    }

    @Test
    public void test_invalidHl7_returnsNull() {
        xmlMessageHeaderParser = new XmlMessageHeaderParser("invalidxml");

        String id = xmlMessageHeaderParser.getMessageHeaderId();
        String eventCode = xmlMessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }

    @Test
    public void test_emptyMessage_returnsNull() {
        xmlMessageHeaderParser = new XmlMessageHeaderParser("");

        String id = xmlMessageHeaderParser.getMessageHeaderId();
        String eventCode = xmlMessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }

    @Test
    public void test_nullMessage_returnsNull() {
        xmlMessageHeaderParser = new XmlMessageHeaderParser(null);

        String id = xmlMessageHeaderParser.getMessageHeaderId();
        String eventCode = xmlMessageHeaderParser.getEventCode();

        assertNull(id);
        assertNull(eventCode);
    }
}

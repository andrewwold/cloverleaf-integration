package com.varian.integration.converters;

import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Message;
import com.quovadx.cloverleaf.upoc.MessageMetadata;
import com.quovadx.cloverleaf.upoc.PropertyTree;
import com.varian.integration.dto.IntegrationMessage;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IntegrationMessageConverterTest {
    private IntegrationMessageConverter integrationMessageConverter;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() throws CloverleafException {
    }

    @Test
    public void test_nullMessage_ThrowsError() throws CloverleafException {
        exception.expect(IllegalArgumentException.class);
        integrationMessageConverter = new IntegrationMessageConverter(null, null);
    }


    @Test
    public void test_nullUserData() throws CloverleafException {
        Message message = mock(Message.class);

        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenReturn(null);

        exception.expect(IllegalArgumentException.class);
        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());
    }

    @Test
    public void test_userDataThrowsException() throws CloverleafException {
        Message message = mock(Message.class);

        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenThrow(CloverleafException.class);

        exception.expect(IllegalArgumentException.class);
        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());
    }

    @Test
    public void test_errorThrownGettingContent() throws CloverleafException {
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        MockitoAnnotations.initMocks(this);
        when(message.getContent()).thenThrow(CloverleafException.class);
        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(createPropTreeWithMessageId());

        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());

        exception.expect(CloverleafException.class);
        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();
    }

    @Test
    public void test_nullMessageContent() throws CloverleafException {
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        MockitoAnnotations.initMocks(this);
        when(message.getContent()).thenReturn(null);
        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(createPropTreeWithMessageId());

        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());

        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();

        assertNull(integrationMessage.getMessageContents());
    }

    @Test
    public void test_nullMessageMetadata() throws CloverleafException {
        Message message = mock(Message.class);

        MockitoAnnotations.initMocks(this);
        when(message.getUserdata()).thenReturn(new PropertyTree());

        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());

        exception.expect(CloverleafException.class);
        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();

    }

    @Test
    public void test_CloverleafMid_returnedInIntegrationMessageId() throws CloverleafException {
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        MockitoAnnotations.initMocks(this);
        when(message.getContent()).thenReturn("");
        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(createPropTreeWithMessageId());

        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());

        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();

        assertEquals(1, integrationMessage.getIntegrationMessageId());
    }

    @Test
    public void test_StringPropertyWhichDoesntExist() throws CloverleafException {
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        MockitoAnnotations.initMocks(this);
        when(message.getContent()).thenReturn(null);
        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(createPropTreeWithMessageId());

        integrationMessageConverter = new IntegrationMessageConverter(message, new PropertyTree());

        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();

        assertNull(integrationMessage.getStatusId());
        assertNull(integrationMessage.getMessageFormat());
    }

    @Test
    public void test_messageContent_ConvertsTo_IntegrationMessage() throws CloverleafException {
        MessageMetadata mmd = mock(MessageMetadata.class);
        Message message = mock(Message.class);
        message.metadata = mmd;

        MockitoAnnotations.initMocks(this);
        when(message.getContent()).thenReturn("<Bundle />");
        when(message.metadata.getMid()).thenReturn(new int[]{0, 0, 1});
        when(message.getUserdata()).thenReturn(createPropTreeWithMessageId());

        integrationMessageConverter = new IntegrationMessageConverter(message, createUserArgPropTreeWithStatusId());

        IntegrationMessage integrationMessage = integrationMessageConverter.getIntegrationMessage();

        assertEquals("<Bundle />", integrationMessage.getMessageContents());
    }


    private PropertyTree createPropTreeWithMessageId() throws PropertyTree.PropertyTreeException {
        PropertyTree prop = new PropertyTree();
        prop.put("MessageId", "2");

        return prop;
    }

    private PropertyTree createUserArgPropTreeWithStatusId() throws PropertyTree.PropertyTreeException {
        PropertyTree propTree = new PropertyTree();
        propTree.put("StatusId", "InProgress");

        return propTree;
    }
}
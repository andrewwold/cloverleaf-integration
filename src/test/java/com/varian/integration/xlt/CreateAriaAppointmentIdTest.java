package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Xpm;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class CreateAriaAppointmentIdTest {
    private CreateAriaAppointmentId createId;

    @Mock
    private CloverEnv cloverEnv;

    @Mock
    private Xpm xpm;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        createId = new CreateAriaAppointmentId();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_validId() throws CloverleafException {
        String id = (String) createId.xlateString(cloverEnv, xpm, "10000");
        assertThat(id, CoreMatchers.endsWith("@10000"));
    }

    @Test
    public void test_emptyInput() throws CloverleafException {
        String id = (String) createId.xlateString(cloverEnv, xpm, "");
        assertEquals("", id);
    }

    @Test
    public void test_nullInput() throws CloverleafException {
        String id = (String) createId.xlateString(cloverEnv, xpm, null);
        assertNull(id);
    }
}

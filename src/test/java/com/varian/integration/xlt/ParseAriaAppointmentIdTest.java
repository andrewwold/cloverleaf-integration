package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Xpm;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ParseAriaAppointmentIdTest {
    private ParseAriaAppointmentId parser;

    @Mock
    private CloverEnv cloverEnv;

    @Mock
    private Xpm xpm;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        parser = new ParseAriaAppointmentId();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_validId() throws CloverleafException {
        String id = (String) parser.xlateString(cloverEnv, xpm, "2015-10-21T11:40:36Z@1000");
        assertEquals("1000", id);
    }

    @Test
    public void test_randomId() throws CloverleafException {
        String id = (String) parser.xlateString(cloverEnv, xpm, "asdfeafe2183kf@1000");
        assertEquals("1000", id);
    }

    @Test
    public void test_nonDelimitedId() throws CloverleafException {
        String id = (String) parser.xlateString(cloverEnv, xpm, "1000");
        assertEquals("1000", id);
    }

    @Test
    public void test_emptyInput() throws CloverleafException {
        String id = (String) parser.xlateString(cloverEnv, xpm, "");
        assertEquals("", id);
    }

    @Test
    public void test_nullInput() throws CloverleafException {
        String id = (String) parser.xlateString(cloverEnv, xpm, null);
        assertNull(id);
    }
}

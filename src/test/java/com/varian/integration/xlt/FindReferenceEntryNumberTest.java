package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Xpm;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class FindReferenceEntryNumberTest {
    private FindReferenceEntryNumber findReferenceEntryNumber;

    @Mock
    CloverEnv cloverEnv;

    @Mock
    Xpm xpm;

    @Before
    public void before() {
        findReferenceEntryNumber = new FindReferenceEntryNumber();
    }

    @Test
    public void test_ReferenceFoundInMessage() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(xpm.getString(any(String.class))).thenReturn("3");

        int reference = (int)findReferenceEntryNumber.xlateString(cloverEnv, xpm, "Condition/3");

        assertEquals(0, reference);
    }

    @Test
    public void test_ReferenceNotFoundInMessage() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(xpm.getString(any(String.class))).thenReturn("3");

        int reference = (int)findReferenceEntryNumber.xlateString(cloverEnv, xpm, "Condition/5");

        assertEquals(-1, reference);
    }

    @Test
    public void test_ReferenceNameNotFoundInMessage() throws CloverleafException {
        MockitoAnnotations.initMocks(this);
        when(xpm.getString(any(String.class))).thenThrow(CloverleafException.class);

        int reference = (int)findReferenceEntryNumber.xlateString(cloverEnv, xpm, "BadName/5");

        assertEquals(-1, reference);
    }
}

package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Xpm;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ConvertDateTest {
    private ConvertDate convertDate;

    @Mock
    private CloverEnv cloverEnv;

    @Mock
    private Xpm xpm;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        convertDate = new ConvertDate();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_validDate() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "20150910");
        assertEquals("2015-09-10", conversion);
    }

    @Test
    public void test_validDateTimeNoSeconds() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "201507161823");
        assertEquals("2015-07-16T18:23:00Z", conversion);
    }

    @Test
    public void test_validDateTimeWithSeconds() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "20150716182355");
        assertEquals("2015-07-16T18:23:55Z", conversion);
    }

    @Test
    public void test_emptyStringReturnsEmptyString() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "");
        assertEquals("", conversion);
    }

    @Test
    public void test_invalidStringWithLengthOf8ReturnsInput() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "zzzzzzzz");
        assertEquals("zzzzzzzz", conversion);
    }

    @Test
    public void test_nullStringReturnsNull() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, null);
        assertNull(conversion);
    }

    @Test
    public void test_longStringReturnsSelf() throws CloverleafException {
        String conversion = (String) convertDate.xlateString(cloverEnv, xpm, "20150910066");
        assertEquals("20150910066", conversion);
    }

}

package com.varian.integration.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Data
@NoArgsConstructor
@Getter
@Setter
public class SaveResponse {
    private long id;
    private String filename;
    private List<Long> ids;
}


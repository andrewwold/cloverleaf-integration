package com.varian.integration.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntegrationMessage {
    private final int integrationMessageId;
    private final String messageId; //hl7 compatible
    private final String messageContents;
    private final String messageFormat;
    private final String patientId;
    private final String statusId;

    public IntegrationMessage(int integrationMessageId, String messageId, String messageContents, String messageFormat, String PatientId, String StatusId) {
        this.integrationMessageId = integrationMessageId;
        this.messageId = messageId;
        this.messageContents = messageContents;
        this.messageFormat = messageFormat;
        this.patientId = PatientId;
        this.statusId = StatusId;
    }
}
package com.varian.integration.frameworks.retrofit;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import okio.Buffer;
import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;

import java.io.IOException;

public class RetrofitClientProvider {
    private String api_url;

    public RetrofitClientProvider(String api_url) {
        this.api_url = api_url;
    }

    public Retrofit get() {
        return new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }
}

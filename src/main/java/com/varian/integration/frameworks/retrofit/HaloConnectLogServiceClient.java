package com.varian.integration.frameworks.retrofit;


import com.varian.integration.dto.IntegrationMessage;
import com.varian.integration.dto.SaveResponse;
import com.varian.integration.frameworks.retrofit.interfaces.RetrofitHaloConnectLogServiceClient;

import java.io.IOException;

public class HaloConnectLogServiceClient extends ServiceClient{
    private final RetrofitHaloConnectLogServiceClient retrofitClient;

    public HaloConnectLogServiceClient(RetrofitHaloConnectLogServiceClient retrofitClient){
        this.retrofitClient = retrofitClient;
    }

    public SaveResponse logIntegrationMessage(IntegrationMessage integrationMessage) throws Exception {
        return execute(retrofitClient.integrationMessage(integrationMessage));
    }
}

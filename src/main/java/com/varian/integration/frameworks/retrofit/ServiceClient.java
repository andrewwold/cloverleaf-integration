package com.varian.integration.frameworks.retrofit;

import retrofit.Call;

import java.io.IOException;

public abstract class ServiceClient {
    protected <T> T execute(Call<T> call) throws Exception {
        retrofit.Response<T> response = call.execute();
        if (response.isSuccess()) {
            return response.body();
        } else {
            System.out.println("Error executing service call, error body: " + response.errorBody());
            throw new Exception("Error executing service call");
        }
    }
}

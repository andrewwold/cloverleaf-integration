package com.varian.integration.frameworks.retrofit.interfaces;

import com.varian.integration.dto.IntegrationMessage;
import com.varian.integration.dto.SaveResponse;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface RetrofitHaloConnectLogServiceClient {
    @Headers("Authorization: Basic b25jb2xvZ2lzdDpvbmNvbG9naXN0")
    @POST("/integrationLogs/integrationMessage")
    Call<SaveResponse> integrationMessage(@Body IntegrationMessage integrationMessage);
}

package com.varian.integration;

public class XmlMessageHeaderParser implements MessageHeaderParser {
    private final String message;

    public XmlMessageHeaderParser(String message) {
        this.message = message;
    }

    public String getEventCode() {
        if (message != null) {
            int eventIndex = message.indexOf("<event>");
            int codeIndex = message.indexOf("<code ", eventIndex);
            String code = null;

            if (eventIndex >= 0 && codeIndex >= 0) {
                String codeElement = message.substring(codeIndex, message.indexOf("/>", codeIndex));
                int firstQuote = codeElement.indexOf("value=\"") + 7;
                code = codeElement.substring(firstQuote, codeElement.indexOf('\"', firstQuote));
            }
            return code;
        }
        return null;
    }

    public String getMessageHeaderId() {
        if (message != null) {
            int msgHdrIndex = message.indexOf("<MessageHeader ");
            int idIndex = message.indexOf("<id ", msgHdrIndex);
            String id = null;

            if (msgHdrIndex >= 0 && idIndex >= 0) {
                String idElement = message.substring(idIndex, message.indexOf("/>", idIndex));
                int firstQuote = idElement.indexOf("value=\"") + 7;
                id = idElement.substring(firstQuote, idElement.indexOf('\"', firstQuote));
            }
            return id;
        }
        return null;
    }
}

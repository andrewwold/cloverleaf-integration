package com.varian.integration;

public class Hl7MessageHeaderParser implements MessageHeaderParser {
    private final String message;

    public Hl7MessageHeaderParser(String message) {
        this.message = message;
    }

    public String getEventCode() {
        String hl7Delimiter = getHl7Delimiter();
        if (getHl7Delimiter() != null) {
            String[] split = message.split("\\" + hl7Delimiter);
            if (split.length >= 9) {
                return split[8];
            }
        }
        return null;
    }

    public String getMessageHeaderId() {
        String hl7Delimiter = getHl7Delimiter();
        if (hl7Delimiter != null) {
            String[] split = message.split("\\" + hl7Delimiter);
            if (split.length >= 10) {
                return split[9];
            }
        }
        return null;
    }

    private String getHl7Delimiter() {
        if (message != null && message.length() >= 4) {
            return Character.toString(message.charAt(3));
        } else {
            return null;
        }
    }
}

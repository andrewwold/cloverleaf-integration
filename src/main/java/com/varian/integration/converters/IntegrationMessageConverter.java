package com.varian.integration.converters;


import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Message;
import com.quovadx.cloverleaf.upoc.PropertyTree;
import com.varian.integration.dto.IntegrationMessage;

public class IntegrationMessageConverter extends MessageConverter {
    private final PropertyTree userArgs;

    public IntegrationMessageConverter(Message message, PropertyTree userArgs) {
        super(message);
        this.userArgs = userArgs;
    }

    public IntegrationMessage getIntegrationMessage() throws CloverleafException {
        int integrationMessageId = getCloverLeafMessageId();
        String messageId = getMessageId();
        String content = getContent();
        String messageFormat = getMessageFormat(content);
        String statusId = getStatusId();
        //String patientId = msgPropTree.getUserDataString("PatientId");

        return new IntegrationMessage(
                integrationMessageId,
                messageId,
                content,
                messageFormat,
                "",
                statusId
        );
    }

    private String getMessageFormat(String message) {
        if (message != null) {
            if (message.startsWith("<")) {
                return "FHIR";
            } else {
                return "HL7";
            }
        } else {
            return null;
        }
    }

    private String getStatusId() throws PropertyTree.PropertyTreeException {
        return userArgs.getString("StatusId");
    }

    private String getMessageId() throws PropertyTree.PropertyTreeException {
        return getUserDataString("MessageId");
    }
}

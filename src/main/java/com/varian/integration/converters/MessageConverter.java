package com.varian.integration.converters;

import com.google.common.base.Preconditions;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.Message;
import com.quovadx.cloverleaf.upoc.PropertyTree;

public abstract class MessageConverter {
    protected final Message message;
    protected PropertyTree userData;

    protected MessageConverter(Message message) {
        Preconditions.checkArgument(message != null);
        this.message = message;

        setUserDataFromMessage();
        Preconditions.checkArgument(userData != null);
    }

    private void setUserDataFromMessage() {
        try {
            userData = message.getUserdata();
        } catch (CloverleafException cle) {
            userData = null;
        }
    }

    protected PropertyTree getUserData() {
        return userData;
    }

    protected String getUserDataString(String prop) throws PropertyTree.PropertyTreeException {
        return getUserData().getString(prop);
    }

    protected String getContent() throws CloverleafException {
        return message.getContent();
    }

    protected int getCloverLeafMessageId() throws CloverleafException {
        if (message.metadata == null) {
            throw new CloverleafException("Message Metadata is null");
        }
        int[] mid = message.metadata.getMid();
        return mid[2];
    }
}

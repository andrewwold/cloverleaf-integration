package com.varian.integration;

import com.google.common.base.Preconditions;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.varian.integration.converters.IntegrationMessageConverter;
import com.varian.integration.dto.IntegrationMessage;
import com.varian.integration.dto.SaveResponse;
import com.varian.integration.frameworks.retrofit.HaloConnectLogServiceClient;

public class HaloLogProcessor {

    private final IntegrationMessageConverter integrationMessageConverter;
    private final HaloConnectLogServiceClient haloConnectLogServiceClient;

    public HaloLogProcessor(IntegrationMessageConverter integrationMessageConverter, HaloConnectLogServiceClient haloConnectLogServiceClient) {
        Preconditions.checkArgument(integrationMessageConverter != null);
        Preconditions.checkArgument(haloConnectLogServiceClient != null);

        this.integrationMessageConverter = integrationMessageConverter;
        this.haloConnectLogServiceClient = haloConnectLogServiceClient;
    }

    public void processMessage() throws CloverleafException {
        IntegrationMessage im = integrationMessageConverter.getIntegrationMessage();

        try {
            SaveResponse response = haloConnectLogServiceClient.logIntegrationMessage(im);

            if (response.getId() >= 0) {
                System.out.println("HaloConnectLog Reponse: " + response.getId());
            }
        } catch (Exception e) {
            System.out.println("Error in execute: " + e);
        }
    }
}

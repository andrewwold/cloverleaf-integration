package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.XLTString;
import com.quovadx.cloverleaf.upoc.Xpm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertDate extends XLTString {

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public Object xlateString(CloverEnv cloverEnv, Xpm xpm, String inVal)
            throws CloverleafException {
        String newDate = null;

        if (inVal != null) {
            try {
                if (inVal.length() == 8) {
                    newDate = formatDate(inVal, new SimpleDateFormat("yyyyMMdd"));
                } else if (inVal.length() == 12) {
                    newDate = formatTimeStamp(inVal, new SimpleDateFormat("yyyyMMddHHmm"));
                } else if (inVal.length() == 14) {
                    newDate = formatTimeStamp(inVal, new SimpleDateFormat("yyyyMMddHHmmss"));
                } else {
                    newDate = inVal;
                }
            } catch (ParseException pe) {
                System.out.println("Could not parse date: " + inVal);
                newDate = inVal;
            }
        }

        return newDate;
    }

    private String formatDate(String inVal, SimpleDateFormat df) throws ParseException {
        Date date = df.parse(inVal);
        return dateFormat.format(date);
    }

    private String formatTimeStamp(String inVal, SimpleDateFormat df) throws ParseException {
        Date date = df.parse(inVal);
        return timestampFormat.format(date);
    }
}


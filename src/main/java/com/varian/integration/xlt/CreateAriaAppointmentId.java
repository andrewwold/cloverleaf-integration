package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.XLTString;
import com.quovadx.cloverleaf.upoc.Xpm;
import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateAriaAppointmentId extends XLTString {
    private final DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private final String delimiter = "@";

    public Object xlateString(CloverEnv cloverEnv, Xpm xpm, String inVal)
            throws CloverleafException {
        String newId = inVal;

        if (StringUtils.isNotBlank(inVal)) {
            newId = timestampFormat.format(new Date()) + delimiter + inVal;
        }

        return newId;
    }
}


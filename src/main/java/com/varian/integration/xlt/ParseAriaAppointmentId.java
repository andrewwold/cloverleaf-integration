package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.XLTString;
import com.quovadx.cloverleaf.upoc.Xpm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ParseAriaAppointmentId extends XLTString {
    private final DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private final String delimiter = "@";

    public Object xlateString(CloverEnv cloverEnv, Xpm xpm, String inVal)
            throws CloverleafException {
        String retVal = inVal;

        if (inVal != null) {
            String split[] = inVal.split(delimiter);
            if (split.length > 1) {
                retVal = split[1];
            }
        }

        return retVal;
    }
}


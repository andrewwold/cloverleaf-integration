package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.*;

import java.util.*;

public class PrintValues extends XLTStrings {

    public Vector xlateStrings(CloverEnv cloverEnv, Xpm xpm, Vector inVals)
            throws CloverleafException {

        System.out.println("PrintValues: ");
        Vector outputStrings = new Vector();
        Enumeration e = xpm.getInAddresses().elements();
        for (; e.hasMoreElements(); ) {
            Object o = e.nextElement();
            try {
                for (Enumeration e2 = xpm.getStrings(o.toString()).elements(); e2.hasMoreElements(); ) {
                    Object o2 = e2.nextElement();
                    System.out.print("[" + o2 + "] ");

                    outputStrings.add(o2);
                }
            } catch (Exception err) {
                System.out.println("error");
            }

            System.out.println();

        }
        return outputStrings;
    }
}

package com.varian.integration.xlt;

import com.quovadx.cloverleaf.upoc.CloverEnv;
import com.quovadx.cloverleaf.upoc.CloverleafException;
import com.quovadx.cloverleaf.upoc.XLTString;
import com.quovadx.cloverleaf.upoc.Xpm;

public class FindReferenceEntryNumber extends XLTString {

    public Object xlateString(CloverEnv cloverEnv, Xpm xpm, String inVal)
            throws CloverleafException {

        String[] reference = inVal.split("/");

        for (int i = 0; i < 15; i++) {
            try {
                String id = xpm.getString("nm1:Bundle.0.nm1:entry(" + i + ").1.nm1:resource.nm1:" + reference[0] + ".nm1:id.&value");
                if (id.equals(reference[1])) {
                    return i;
                }
            } catch (Exception e) {
            }
        }

        return -1;
    }
}


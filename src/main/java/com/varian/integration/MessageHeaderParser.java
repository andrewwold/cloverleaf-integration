package com.varian.integration;

public interface MessageHeaderParser {
    String getMessageHeaderId();
    String getEventCode();
}

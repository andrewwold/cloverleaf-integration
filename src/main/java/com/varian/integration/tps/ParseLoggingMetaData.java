package com.varian.integration.tps;

import com.quovadx.cloverleaf.upoc.*;
import com.varian.integration.Hl7MessageHeaderParser;
import com.varian.integration.MessageHeaderParser;
import com.varian.integration.XmlMessageHeaderParser;

public class ParseLoggingMetaData extends com.quovadx.cloverleaf.upoc.TPS {

    public ParseLoggingMetaData(CloverEnv cloverEnv, PropertyTree xArgs)
            throws CloverleafException {
        super(cloverEnv, xArgs);
        extractAndValidateUserArguments(cloverEnv, xArgs);
    }

    public DispositionList process(CloverEnv cloverEnv, String context, String mode, Message msg)
            throws CloverleafException {
        DispositionList dispList = new DispositionList();

        if ("run".equals(mode)) {
            setUserDataFromMessageHeader(msg);
        }

        dispList.add(DispositionList.CONTINUE, msg);
        return dispList;
    }

    private void setUserDataFromMessageHeader(Message msg) throws CloverleafException {
        if (msg != null) {
            String messageData = msg.getContent(0, 1024); //offset, length
            if (isXMLFormat(msg.getContent(0, 2))) {
                setMessageUserData(msg, new XmlMessageHeaderParser(messageData));
            } else {
                setMessageUserData(msg, new Hl7MessageHeaderParser(messageData));
            }
        }
    }

    private void setMessageUserData(Message msg, MessageHeaderParser messageHeaderParser) throws CloverleafException {
        PropertyTree msgPropTree = buildPropertyTree(msg, messageHeaderParser);
        msg.setUserdata(msgPropTree);
    }

    private PropertyTree buildPropertyTree(Message msg, MessageHeaderParser messageHeaderParser) throws CloverleafException {
        PropertyTree msgPropTree = msg.getUserdata();
        msgPropTree.put("EventCode", messageHeaderParser.getEventCode());
        msgPropTree.put("MessageId", messageHeaderParser.getMessageHeaderId());
        return msgPropTree;
    }

    private boolean isXMLFormat(String message) {
        return message != null && message.trim().startsWith("<");
    }

    protected void destroy(CloverEnv cloverEnv) throws CloverleafException {
    }

}




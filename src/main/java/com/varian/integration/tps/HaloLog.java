package com.varian.integration.tps;

import com.quovadx.cloverleaf.upoc.*;
import com.varian.integration.HaloLogProcessor;
import com.varian.integration.converters.IntegrationMessageConverter;
import com.varian.integration.frameworks.retrofit.HaloConnectLogServiceClient;
import com.varian.integration.frameworks.retrofit.RetrofitClientProvider;
import com.varian.integration.frameworks.retrofit.interfaces.RetrofitHaloConnectLogServiceClient;

/**
 * Type:     JavaTps
 * Purpose:  Write description here.
 */

public class HaloLog extends com.quovadx.cloverleaf.upoc.TPS {
    public static final String API_URL = "http://engsandbox-docker-halo.hdev.in:41001";//"http://10.26.104.52:8001";
    private RetrofitClientProvider retrofitClientProvider;

    public HaloLog(CloverEnv cloverEnv, PropertyTree xArgs)
            throws CloverleafException {
        super(cloverEnv, xArgs);
        extractAndValidateUserArguments(cloverEnv, xArgs);
        retrofitClientProvider = new RetrofitClientProvider(API_URL);
    }

    public DispositionList process(
            CloverEnv cloverEnv,
            String context,
            String mode,
            Message msg)
            throws CloverleafException {
        // Initialise our return value
        DispositionList dispList = new DispositionList();

        if ("run".equals(mode)) {
            HaloConnectLogServiceClient haloConnectLogServiceClient = getHaloConnectLogServiceClient();

            IntegrationMessageConverter imc = new IntegrationMessageConverter(msg, userArgs);
            HaloLogProcessor haloLogProcessor = new HaloLogProcessor(imc, haloConnectLogServiceClient);
            haloLogProcessor.processMessage();
        }

        dispList.add(DispositionList.CONTINUE, msg);
        return dispList;
    }

    private HaloConnectLogServiceClient getHaloConnectLogServiceClient() {
        return new HaloConnectLogServiceClient(retrofitClientProvider.get()
                .create(RetrofitHaloConnectLogServiceClient.class));
    }

    protected void destroy(CloverEnv cloverEnv) throws CloverleafException {
    }

}
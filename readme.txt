To run:
gradlew run

To build and test:
gradlew build

To build the fat jar:
gradlew shadow

To test:
gradlew test

To open in IDEA:
- Launch IDEA
- Import Project and select build.gradle
- Check the option File -> Settings -> Compiler -> Annotation Processors -> Default -> Enable annotation processing
- Go to File -> Settings -> Plugins, search for and install "Lombok Plugin"
- Run or debug ./src/main/java/com/varian/services/Application.java